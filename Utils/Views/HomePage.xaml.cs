﻿using System;
using System.Collections.Generic;
using Utils.ViewModels;
using Xamarin.Forms;

namespace Utils.Views
{
    public partial class HomePage : ContentPage {
        public HomePage() {
            InitializeComponent();
        }
        private async void OnTapGestureRecognisePhone(object sender, EventArgs e) {  
            await Navigation.PushAsync(new PhoneButton());  
        } 
        
        private async void OnTapGestureRecogniseCalculator(object sender, EventArgs e) {  
            await Navigation.PushAsync(new CalcPage());  
        } 
        
       
    }
}
